﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorCloser : MonoBehaviour
{
    public GameObject _door;
    public Transform _originalPosition;
    private bool _doorIsOpen = true;


    private void OnTriggerEnter(Collider other)
    {
        if (_doorIsOpen)
        {
        Debug.Log("Trigger entered");
            _door.transform.rotation = _originalPosition.transform.rotation;
            _doorIsOpen = false;
        }

        else
        {
            Debug.Log("Door is already closed");
        }
    }
}
