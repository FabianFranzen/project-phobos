﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClownSpawner : MonoBehaviour
{
    public GameObject _objectToSpawn;

    private void OnTriggerEnter(Collider other)
    {
        EnableClown();
    }

    public void EnableClown()
    {
        _objectToSpawn.SetActive(true);
    }
}
