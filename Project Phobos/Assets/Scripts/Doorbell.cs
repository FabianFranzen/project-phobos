﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Doorbell : MonoBehaviour
{
    public AudioSource _audioSource;
    public AudioClip _audioClip;

    private void OnTriggerEnter(Collider other)
    {
        _audioSource.Play();
        Debug.Log("Triggered");
        
    }
}
