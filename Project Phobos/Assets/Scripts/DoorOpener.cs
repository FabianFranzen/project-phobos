﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    public Animator _animator;
    public GameObject _door;
    public GameObject _doorSound;
    private bool _doorIsOpen = false;

    void Start()
    {
        //_animator = GetComponent<Animator>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!_doorIsOpen)
        {

            StartCoroutine(Time());


        }

        else
        {
            Debug.Log("Door is already Open");
        }
    }

    IEnumerator Time()
    {
        yield return new WaitForSeconds(3);
        _animator.SetBool("Open", true);

        Debug.Log(_animator);
        _doorSound.SetActive(true);
        _doorIsOpen = true;
    }

}
