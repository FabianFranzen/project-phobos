﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMovement : MonoBehaviour
{
    private float _movementSpeed = 0.5f;
    public Transform _player;
    private Vector3 _playerDirection;


    private void FixedUpdate()
    {
        transform.LookAt(_player);
        _playerDirection = _player.position - transform.position;
        transform.Translate(0f, 0f, _movementSpeed * Time.deltaTime);


        // Debug.Log(_playerDirection);
    }




}
